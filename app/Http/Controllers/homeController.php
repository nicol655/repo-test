<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use View;

class homeController extends Controller
{
    function index(){
        return View::make('home.home_content');
    }

    function contact(){
        return View::make('home.contact_content');
    }
}