*********************** HOME VIEW W/O SIGNED IN ***********************

SELECT
	vi.id AS dedi_id,
	vi.route_miniature AS route_mini,
	vi.name AS dedi_name,
	v_l.id AS video_id,
	v_l.route_video,
	v_l.name AS video_name,
	lenguage.reference AS lenguage,
	tags.reference AS tag
FROM videos AS vi
    INNER JOIN tags_videos AS t_g
        ON vi.id = t_g.videos_id

    INNER JOIN reference AS tags
        ON t_g.category_id = tags.id

    INNER JOIN videos_lenguage AS v_l
        ON vi.id = v_l.videos_id

    INNER JOIN reference AS lenguage
        ON v_l.lenguage_id = lenguage.id

*********************** TOTALS COUNTS AND SUMS FOR VIDEOS ***********************

SELECT
	videos_lenguage.id AS video_id,
	videos_lenguage.name AS Name,
	IFNULL(usability_down.aux,0) AS Downloads,
	IFNULL(usability_repo.aux,0) AS Reproductions,
	IFNULL(usability_like.aux,0) AS Likes,
	COUNT(se.id) + COUNT(rh.id) AS Shares
FROM videos
	INNER JOIN videos_lenguage
		ON videos.id = videos_lenguage.videos_id

	LEFT JOIN usability AS usability_down
		ON videos_lenguage.id = usability_down.videos_lenguage_id
		AND usability_down.type_id =
			(SELECT id FROM reference WHERE reference = "Descargado")

	LEFT JOIN usability AS usability_repo
		ON videos_lenguage.id = usability_repo.videos_lenguage_id
		AND usability_repo.type_id =
			(SELECT id FROM reference WHERE reference = "Reproducido")

	LEFT JOIN usability AS usability_like
		ON videos_lenguage.id = usability_like.videos_lenguage_id
		AND usability_like.type_id =
			(SELECT id FROM reference WHERE reference = "Like")

    LEFT JOIN events AS e
    	ON e.videos_lenguage_id = videos_lenguage.id
	LEFT JOIN social_events AS se
        ON e.id = se.events_id

	LEFT JOIN sending_history AS sh
    	ON sh.videos_lenguage_id = videos_lenguage.id
	LEFT JOIN receiver_history AS rh
        ON sh.id = rh.video_history_id

GROUP BY video_id

*********************** STORED PROCEDURE OF donwloads_increment ***********************

DELIMITER //
CREATE PROCEDURE repro_increment(idVideo INT)

    BEGIN
        DECLARE cond INT DEFAULT 0;
        SELECT COUNT(*) INTO cond FROM usability WHERE videos_lenguage_id = idVideo
            AND type_id = 78;
        IF cond = 0 THEN
            INSERT INTO usability (user_id, videos_lenguage_id, type_id, aux)
                VALUES (null, idVideo, 78, 1);
        ELSEIF cond > 0 THEN
            UPDATE usability SET aux = aux + 1 WHERE videos_lenguage_id = idVideo
                AND type_id = 78;
        END IF;
    END

//
DELIMITER ;