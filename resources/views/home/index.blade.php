<!DOCTYPE html>
<html lang="en">
<head>
    @include('home.layout._head')
    <!-- start: AngularJS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>

</head>
<body>

    @include('home.layout._header')

    <div class="container-fluid mimin-wrapper">
        @include('home.layout._sidebar')
        @yield('content')
    </div>

    @include('home.layout._footer')

</body>
</html>