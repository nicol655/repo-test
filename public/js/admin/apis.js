

var app = angular.module('apisAPP', []);

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
})

app.controller('apisCTL', ['$scope', '$http', '$location',
function($scope, $http, $location) {
    $scope.init = function(){
        $scope.title = "API's";
    };
}]);
angular.bootstrap($("#ngApp3"), ['apisAPP']);